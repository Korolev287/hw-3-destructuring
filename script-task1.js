// Деструктуризация — это разложение сложной структуры (объекта, массива, функции) 
// на простые части (свойства, элементы, составляющие) в целях получения к ним 
// доступа более эффективным способом.

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
const allClients = [...clients1, ...clients2];

let finalClientObj = new Set(allClients);
let finalClientArr = [];

for (let value of finalClientObj) {finalClientArr.push(value)};

console.log(finalClientArr);