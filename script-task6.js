const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
  }
  
const age = 30;
const salary = 150;

const newEmployee = {...employee, age, salary}
console.log(newEmployee);